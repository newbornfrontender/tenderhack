const {
  override,
  addPostcssPlugins,
  addBabelPlugin,
} = require('customize-cra');

const env = process.env.NODE_ENV;
const isProd = env === 'production';

const plugins = [
  require('postcss-use')(),
  require('postcss-preset-env')({
    stage: 0,
    autoprefixer: isProd,
  }),
].filter(Boolean);
const postcss = addPostcssPlugins([require('postcss-normalize')(), ...plugins]);

const babel = addBabelPlugin([
  require('reshadow/babel')({
    postcss: plugins,
  }),
]);

module.exports = (config) => {
  config.module.rules = [
    {
      test: /\.(js|mjs|jsx|ts|tsx)$/,
      exclude: /node_modules/,
      use: ['reshadow/webpack/loader', 'babel-loader'],
    },
    ...config.module.rules,
  ];

  return override(postcss, babel)(config);
};
