import axios from 'axios';

export const multipart = {
  headers: { 'Content-Type': 'multipart/form-data' },
};

export default {
  get: (url, config) => axios.get(url, config),
  post: (url, data, config) => axios.post(url, data, config),
};
