import React, { useState } from 'react';
import styled from 'reshadow';
import { FileUploader } from './file-uploader';
import { Table } from './table';
import { Aside } from './aside';
import { TableWrapper } from './table-wrapper';
import { Footer } from './footer';
import { TableParameters } from './table-parameters';
import { TableInfo } from './table-info';
import { IndicatorsInfo } from './indicators-info';
import { ContentInfo } from './content-info';
import { Button } from './button';
import { Select } from './select';
import { CustomField } from './custom-field';
import { Categories } from './categories';
import axios, { multipart } from '../api';

export const App = () => {
  const [reqestId, setReqestId] = useState(null);
  const [grid, setGrid] = useState(null);
  const [isPatterSelected, setIsPatterSelected] = useState(false);
  const [row, setRow] = useState('20');
  const [col, setCol] = useState('50');
  const [selectedTable, setSelectedTable] = useState(null);
  const [isCustomFieldVisible, setIsCustomFieldVisible] = useState(false);
  const [customFieldValue, setCustomFieldValue] = useState('');
  const [uploadLink, setUploadLink] = useState(null);
  const [categories, setCategories] = useState({
    name: '',
    company: '',
    url: '',
    platform: '',
    version: '',
    agency: '',
    email: '',
  });

  const handleChange = (e) => {
    const value = e.target.value;
    setCategories({
      ...categories,
      [e.target.name]: value,
    });
  };

  const handleFileChange = (e) => {
    e.preventDefault();

    const formData = new FormData();
    const file = e.target.files[0];

    formData.append('table_file', file);

    axios.post('/api/convert-requests/', categories).then(({ data }) => {
      setReqestId(data.id);

      axios
        .post(
          `/api/convert-requests/${data.id}/upload-table/`,
          formData,
          multipart,
        )
        .then(() => {
          axios
            .get('/api/get-range/', {
              params: {
                request_id: data.id,
                row_start: 0,
                row_end: 20,
                col_start: 0,
                col_end: 50,
              },
            })
            .then(({ data }) => setGrid(data));
        });
    });
  };

  const hanldeOfferClick = () => {
    axios
      .get('/api/get-range/', {
        params: {
          request_id: reqestId,
          row_start: 0,
          row_end: row,
          col_start: 0,
          col_end: col,
        },
      })
      .then(({ data }) => setGrid(data));
  };

  const handlePatternClick = () => {
    axios
      .post('/api/create-first-offer/', {
        request_id: reqestId,
        ...selectedTable,
      })
      .then(({ data }) => {
        setIsPatterSelected(true);
        setGrid(data);
      });
  };

  const handleSelectChange = (value, label) => {
    const { row_start, row_end, col_start, col_end } = selectedTable;
    const oneCellSelected =
      row_end === row_start + 1 && col_end === col_start + 1;
    const newGrid = [...grid];

    if (oneCellSelected) {
      if (value === 'custom') {
        setIsCustomFieldVisible(true);
        return;
      }

      newGrid[row_start][col_start]['prop'] = value;
      newGrid[row_start][col_start]['label'] = label;
      newGrid[row_start][col_start]['selected'] = 'user';
      setGrid(newGrid);

      // for (let i = 0; i < grid.length; i++) {
      //   for (let j = 0; j < grid[i].length; j++) {
      //     if (grid[i][j].label)
      //       arr.push({
      //         convert_id: reqestId,
      //         col: j,
      //         row: i,
      //         prop: grid[i][j].prop,
      //         prop_name: grid[i][j].prop === 'custom' ? grid[i][j].label : '',
      //       });
      //   }
      // }

      const item = {
        convert_id: reqestId,
        col: col_start,
        row: row_start,
        prop: newGrid[row_start][col_start].prop,
        prop_name:
        newGrid[row_start][col_start].prop === 'custom'
            ? newGrid[row_start][col_start].label
            : '',
      }

      axios.post('/api/static-patterns/', item);
    }
  };

  const handleFinishClick = () => {
    const arr = [];

    for (let i = 0; i < grid.length; i++) {
      for (let j = 0; j < grid[i].length; j++) {
        if (grid[i][j].label)
          arr.push({
            convert_id: reqestId,
            col: j,
            row: i,
            prop: grid[i][j].prop,
            prop_name: grid[i][j].prop === 'custom' ? grid[i][j].label : '',
          });
      }
    }

    // Promise.all(
    //   arr.map((item) => axios.post('/api/static-patterns/', item)),
    // ).then(() => {
    //   axios
    //     .post(`/api/convert-requests/${reqestId}/trigger/`, {})
    //     .then(({ data }) => setUploadLink(data.output_file));
    // });

    axios
      .post(`/api/convert-requests/${reqestId}/trigger/`, {})
      .then(({ data }) => setUploadLink(data.output_file));
  };

  const handleCustomFieldClick = () => {
    const { row_start, col_start } = selectedTable;
    const newGrid = [...grid];

    setIsCustomFieldVisible(false);
    setCustomFieldValue('');

    newGrid[row_start][col_start]['prop'] = 'custom';
    newGrid[row_start][col_start]['label'] = customFieldValue;
    newGrid[row_start][col_start]['selected'] = 'user';
    setGrid(newGrid);

    const item = {
      convert_id: reqestId,
      col: col_start,
      row: row_start,
      prop: newGrid[row_start][col_start].prop,
      prop_name:
      newGrid[row_start][col_start].prop === 'custom'
          ? newGrid[row_start][col_start].label
          : '',
    }

    axios.post('/api/static-patterns/', item);
  };

  const handleDynamicPatternClick = () => {
    axios
      .post('/api/dynamic-patterns/', {
        convert_id: reqestId,
        ...selectedTable,
      })
      .then(({ data }) => {
        const newGrid = [...grid];
        const { col_start, col_end, row_start, row_end } = data;

        for (let i = row_start; i < row_end; i++) {
          newGrid[i][col_start].label = 'свойство';
          newGrid[i][col_end - 1].label = 'значение';
          newGrid[i][col_start].selected = 'user';
          newGrid[i][col_end - 1].selected = 'user';
        }
        setGrid(newGrid);
      });
  };

  const footer = styled(null)`
    content {
      display: flex;
      justify-content: space-between;
      inline-size: 100%;
    }
  `(
    <Footer>
      <content>
        {isPatterSelected ? (
          <TableInfo grid={grid} />
        ) : (
          <TableParameters
            rowValue={row}
            onChangeRow={setRow}
            colValue={col}
            onChangeCol={setCol}
            onClick={hanldeOfferClick}
          />
        )}
        <Button size="s" type="danger" onClick={handleDynamicPatternClick}>
          Указать динамический паттерн
        </Button>
      </content>
    </Footer>,
  );

  return styled(null)`
    content {
      min-block-size: 100vh;
    }

    Select {
      margin-block-end: 10px;
    }
  `(
    <main>
      <content>
        {!grid ? (
          <>
            <FileUploader onChange={handleFileChange} />
            <Categories categories={categories} handleChange={handleChange} />
          </>
        ) : !uploadLink ? (
          <TableWrapper>
            <Table
              grid={grid}
              footer={grid ? footer : null}
              onSelect={setSelectedTable}
            />
            <Aside>
              {!isPatterSelected ? (
                <ContentInfo
                  title="Выделите первую оферту в таблице"
                  description="Выделите все ячейки, относящиеся к первому элементу, чтобы система смогла найти соответствия"
                >
                  <Button
                    size="m"
                    disabled={!selectedTable}
                    onClick={handlePatternClick}
                    type="danger"
                  >
                    Задать оферту
                  </Button>
                </ContentInfo>
              ) : (
                <>
                  <ContentInfo
                    title="Укажите ячейки, содержащие обязательные элементы"
                    description="Выделите ячейки, которые содержат значения обязательных элементов, затем задайте им свойство"
                  >
                    <Select onChange={handleSelectChange} />
                    {isCustomFieldVisible && (
                      <CustomField
                        value={customFieldValue}
                        onChange={setCustomFieldValue}
                        onClick={handleCustomFieldClick}
                      />
                    )}
                    <Button size="m" onClick={handleFinishClick} type="danger">
                      Далее
                    </Button>
                  </ContentInfo>
                  <IndicatorsInfo />
                </>
              )}
            </Aside>
          </TableWrapper>
        ) : (
          <a rel="noopener noreferrer" target="_blank" href={uploadLink}>
            Ссылка для скачивания
          </a>
        )}
      </content>
    </main>,
  );
};
