import React from 'react';
import styled from 'reshadow';

export const Aside = ({ children }) => {
  return styled(null)`
    aside {
      display: flex;
      flex-direction: column;
      inline-size: var(--aside-width);
      min-block-size: 100vh;
      max-block-size: 100vh;
      padding: 48px 40px 0;
      border-left: 1px solid var(--border-color);
      background-color: var(--white);
      overflow-y: auto;
    }
  `(<aside>{children}</aside>);
};
