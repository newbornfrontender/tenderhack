import React from 'react';
import styled, { use } from 'reshadow';

export const Button = ({ children, size, onClick, type, disabled }) => {
  return styled(null)`
    button {
      background-color: var(--white);
      font-weight: 700;
      font-size: 15px;

      &[disabled] {
        color: var(--grey) !important;
        background-color: var(--interactive-disabled) !important;
      }

      &[use|type] {
        &=danger {
          color: var(--white);
          background-color: var(--interactive-danger);
        }
      }

      &[use|size] {
        &=s {
          block-size: 40px;
          padding: 0 var(--button-inline-indent-s);
        }

        &=m {
          block-size: 48px;
          padding: 0 var(--button-inline-indent-m);
        }
      }
    }
  `(
    <button
      onClick={onClick}
      {...use({ size, type })}
      type="button"
      disabled={disabled}
    >
      {children}
    </button>,
  );
};
