import React from 'react';
import styled from 'reshadow';

const list = [
  {
    name: 'name',
    type: 'О',
    desc:
      'Короткое название магазина, не более 20 символов. Название магазина должно совпадать с фактическим названием магазина.',
  },
  {
    name: 'company',
    type: 'О',
    desc:
      'Полное наименование компании, владеющей магазином. Не публикуется, используется для внутренней идентификации.',
  },
  {
    name: 'url',
    type: 'Н',
    desc:
      'URL главной страницы магазина. Максимум 50 символов. Допускаются кириллические ссылки.',
  },
  {
    name: 'platform',
    type: 'Н',
    desc:
      'Система управления контентом, на основе которой работает магазин (CMS).',
  },
  {
    name: 'version',
    type: 'Н',
    desc: 'Версия CMS.',
  },
  {
    name: 'agency',
    type: 'Н',
    desc:
      'Наименование агентства, которое оказывает техническую поддержку магазину и отвечает за работоспособность сайта.',
  },
  {
    name: 'email',
    type: 'Н',
    desc:
      'Контактный адрес разработчиков CMS или агентства, осуществляющего техподдержку.',
  },
];

const Row = ({ categories, onChange, name, type, desc }) => {
  return styled(null)`
    td {
      border: 1px solid var(--black);
    }
  `(
    <tr>
      <td>{name}</td>
      <td>
        <input
          type="text"
          name={name}
          value={categories[name]}
          onChange={onChange}
        />
      </td>
      <td>{type}</td>
      <td>{desc}</td>
    </tr>,
  );
};

export const Categories = ({ categories, handleChange }) => {
  return styled(null)`
    table {
      border-collapse: collapse;
      background-color: var(--white);
    }
  `(
    <table>
      <tbody>
        {list.map(({ name, type, desc }, i) => (
          <Row
            key={i}
            type={type}
            name={name}
            desc={desc}
            categories={categories}
            onChange={handleChange}
          />
        ))}
      </tbody>
    </table>,
  );
};
