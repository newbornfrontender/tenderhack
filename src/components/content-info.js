import React from 'react';
import styled from 'reshadow';

export const ContentInfo = ({ title, description, children }) => {
  return styled(null)`
    section {
      margin-block-end: 80px;
    }

    h2 {
      margin-block-end: 20px;
      font-size: 28px;
      line-height: 32px;
      font-weight: 700;
    }

    p {
      margin-block-end: 26px;
      font-size: 15px;
      line-height: 22px;
    }
  `(
    <section>
      <h2>{title}</h2>

      <p>{description}</p>

      {children}
    </section>,
  );
};
