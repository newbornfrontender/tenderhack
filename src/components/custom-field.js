import React from 'react';
import styled from 'reshadow';

export const CustomField = ({ onChange, value, onClick }) => {
  return styled(null)`
    content {
      margin-block-end: 40px;
    }

    input {
      border: 1px solid;
    }
  `(
    <content>
      <input onChange={(e) => onChange(e.target.value)} value={value} />
      <button type="button" onClick={onClick}>
        Указать
      </button>
    </content>,
  );
};
