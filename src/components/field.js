import React from 'react';
import styled from 'reshadow';

export const Field = ({ onChange, value, maxLength }) => {
  return styled(null)`
    input {
      block-size: 32px;
      inline-size: 56px;
      text-align: center;
      font-size: 15px;
      font-weight: 600;
      background-color: #e9eef4;
    }
  `(
    <input
      onChange={onChange}
      value={value}
      maxLength={maxLength}
      type="text"
    />,
  );
};
