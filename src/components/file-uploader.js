import React from 'react';
import styled from 'reshadow';

export const FileUploader = ({ onChange }) => {
  return styled(null)`
    input {
      margin-block-end: 20px;
    }
  `(<input type="file" onChange={onChange} />);
};
