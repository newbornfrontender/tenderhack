import React from 'react';
import styled from 'reshadow';

export const Footer = ({ children }) => {
  return styled(null)`
    content {
      display: flex;
      align-items: center;
      min-block-size: var(--table-footer-height);
      margin-block-start: auto;
      padding: 0 var(--base-indent);
      border-top: 1px solid var(--border-color);
      background-color: var(--white);
    }
  `(<content>{children}</content>);
};
