import React from 'react';
import styled from 'reshadow';

export const Image = ({ src, alt = '' }) => {
  return styled(null)`
    img {
      max-inline-size: 50px;
      max-block-size: 50px;
    }
  `(<img alt={alt} src={src} />);
};
