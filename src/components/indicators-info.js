import React from 'react';
import styled from 'reshadow';
import { Info } from './info';

export const IndicatorsInfo = () => {
  return styled(null)`
    content {
      margin-block-start: auto;
      margin-block-end: var(--table-footer-height);
    }

    h3 {
      margin-block-end: var(--base-indent);
      font-size: 20px;
      line-height: 24px;
      font-weight: bold;
    }
  `(
    <content>
      <h3>Значение индикации</h3>

      <Info size="s" theme="red" bottomIndent="8px">
        Требуется определение
      </Info>
      <Info size="s" theme="yellow" bottomIndent="8px">
        Определено автоматически
      </Info>
      <Info size="s" theme="green" bottomIndent="8px">
        Определено пользователем
      </Info>
      <Info size="s" theme="blue">
        Активное выделение
      </Info>
    </content>,
  );
};
