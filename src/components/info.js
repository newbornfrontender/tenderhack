import React from 'react';
import styled, { use } from 'reshadow';

export const Info = ({ children, theme, size, bottomIndent = '0px' }) => {
  return styled(null)`
    p {
      display: flex;
      align-items: center;
      inline-size: fit-content;
      margin-block-end: ${bottomIndent};
      border: 1px solid #dbe0e6;
      font-weight: 600;
      background-color: var(--white);

      &[use|theme] {
        &=red {
          border-color: var(--info-red);
          background-color: var(--info-red-lighter);
        }

        &=yellow {
          border-color: var(--info-yellow);
          background-color: var(--info-yellow-lighter);
        }

        &=green {
          border-color: var(--info-green);
          background-color: var(--info-green-lighter);
        }

        &=blue {
          border-color: var(--info-blue);
          background-color: var(--info-blue-lighter);
        }
      }

      &[use|size] {
        &=s {
          block-size: 32px;
          padding: 0 var(--info-inline-indent-s);
          font-size: 13px;
        }

        &=m {
          block-size: 40px;
          padding: 0 var(--info-inline-indent-m);
          font-size: 15px;
        }
      }
    }
  `(<p {...use({ theme, size })}>{children}</p>);
};
