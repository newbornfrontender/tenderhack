import React from 'react';
import styled, { use } from 'reshadow';

export const LabeledCell = ({ children, label, selected }) => {
  return styled(null)`
    content {
      display: flex;
      flex-direction: column;
      align-items: end;
      block-size: 100%;
      padding: 20px !important;

      &[use|selected] {
        &=user {
          background-color: var(--info-green-lighter);
        }
      }
    }

    p {
      inline-size: fit-content;
      margin-block-start: -20px;
      margin-inline-start: -20px;
      padding: 2px 4px;

      &[use|selected] {
        &=user {
          background-color: var(--info-green);
        }
      }
    }
  `(
    <content {...use({ selected })}>
      {label && <p {...use({ selected })}>{label}</p>}

      {children}
    </content>,
  );
};
