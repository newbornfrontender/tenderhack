import React from 'react';
import styled from 'reshadow';
import CustomSelect from 'react-select';
import { components } from 'react-select';

const options = [
  {
    value: 'name',
    label: 'Имя товара',
  },
  {
    value: 'beginDate',
    label: 'Дата начала предложения',
  },
  {
    value: 'endDate',
    label: 'Дата окончания предложения',
  },
  {
    value: 'price',
    label: 'Цена товара',
  },
  {
    value: 'model',
    label: 'Модель товара',
  },
  {
    value: 'vendor',
    label: 'Название производителя',
  },
  {
    value: 'vat',
    label: 'Ставка НДС товара',
  },
  {
    value: 'custom',
    label: 'Пользовательский ввод',
  },
];

const customStyles = {
  container: (styles) => ({
    ...styles,
    width: '100%',
  }),

  control: (styles) => ({
    ...styles,
    backgroundColor: 'var(--white)',
    height: '40px',
    borderRadius: 0,
    width: '100%',
    fontSize: '15px',
    color: '#8f9aaa',
    border: '2px solid #w6wbf2',
    '&:focus': {
      border: '2px solid #e6ebf2 !important',
    },
  }),

  indicatorSeparator: (styles) => ({
    ...styles,
    display: 'none',
  }),

  menu: (styles) => ({
    ...styles,
    borderRadius: 0,
    backgroundColor: 'var(--white)',
  }),

  option: (styles) => ({
    ...styles,
    fontSize: '16px',
  }),
};

const CaretDownIcon = ({ isOpen }) => {
  return (
    <svg
      width="10"
      height="5"
      viewBox="0 0 9 4"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      style={{ transform: !isOpen && 'rotate(180deg)' }}
    >
      <path
        d="M4.4496 -3.97809e-07L0 4L9 4L4.4496 -3.97809e-07Z"
        fill="#8f9aaa"
      />
    </svg>
  );
};

const DropdownIndicator = (props) => {
  return (
    <components.DropdownIndicator {...props}>
      <CaretDownIcon isOpen={props.selectProps.menuIsOpen} />
    </components.DropdownIndicator>
  );
};

export const Select = ({ onChange }) => {
  return styled(null)`
    CustomSelect {
      margin-block-end: 40px;
    }
  `(
    <CustomSelect
      options={options}
      onChange={({ value, label }) => onChange(value, label)}
      styles={customStyles}
      components={{ DropdownIndicator }}
    />,
  );
};
