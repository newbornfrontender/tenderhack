import React from 'react';
import styled from 'reshadow';
import { Info } from './info';

const getCellsQuantity = (table) => {
  let count = 0;

  for (let i = 0; i < table.length; i++) {
    for (let j = 0; j < table[i].length; j++) {
      if (table[i][j].val) count += 1;
    }
  }

  return count;
};

const text = (table) => {
  const num = getCellsQuantity(table);
  const str = num.toString();
  const lastNum = str.charAt(str.length - 1);

  if (lastNum === '1') return 'элемент';
  if (Number(lastNum) > 1 && Number(lastNum) <= 4) return 'элемента';
  return 'элементов';
};

export const TableInfo = ({ grid }) => {
  return styled(null)``(
    <Info size="m">
      Таблица разбита на {getCellsQuantity(grid)} {text(grid)}
    </Info>,
  );
};
