import React from 'react';
import styled from 'reshadow';
import { Button } from './button';
import { Field } from './field';

export const TableParameters = ({
  onChangeRow,
  onChangeCol,
  rowValue,
  colValue,
  onClick,
}) => {
  return styled(null)`
    content {
      display: flex;
      align-items: center;
    }

    div {
      margin-inline-end: 24px;
      font-weight: 600;
      font-size: 15px;
      line-height: 20px;
    }
  `(
    <content>
      <div>
        Строк:{' '}
        <Field
          onChange={(e) => onChangeRow(e.target.value)}
          value={rowValue}
          maxLength={3}
        />{' '}
        Столбцов:{' '}
        <Field
          onChange={(e) => onChangeCol(e.target.value)}
          value={colValue}
          maxLength={3}
        />
      </div>

      <Button size="s" type="danger" onClick={onClick}>
        Применить
      </Button>
    </content>,
  );
};
