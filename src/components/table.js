import React from 'react';
import styled from 'reshadow';
import ReactDataSheet from 'react-datasheet';
import { Image } from './image';
import { LabeledCell } from './labeled-cell';
import 'react-datasheet/lib/react-datasheet.css';

const addOptionsToCell = (grid, options = {}) => {
  return grid.map((row) => row.map((cell) => ({ ...cell, ...options })));
};

export const Table = ({ grid, footer, onSelect }) => {
  const handleSelect = ({ start, end }) => {
    onSelect({
      row_start: start.i,
      row_end: end.i + 1,
      col_start: start.j,
      col_end: end.j + 1,
    });
  };

  return styled(null)`
    :global(.read-only) {
      color: var(--black) !important;
      text-align: initial !important;
      background: initial !important;
      box-sizing: content-box;
    }

    :global(.cell) {
      min-inline-size: 150px;
      max-inline-size: 350px;
      padding: 0 !important;
      border-color: var(--border-color);
      font-weight: 600;
      font-size: 13px;
      line-height: 16px;
      vertical-align: middle !important;
    }

    :global(.value-viewer) {
      block-size: 100%;
    }

    :global(.cell.selected) {
      box-shadow: none !important;
      background-color: #e4f0fd !important;
      border-color: #bdd0e6 !important;
    }

    :global(.value-viewer) {
      inline-size: max-content;
      max-inline-size: inherit;
    }

    container {
      display: flex;
      flex-direction: column;
      inline-size: calc(100% - var(--aside-width));
    }

    content {
      block-size: fit-content;
      max-block-size: calc(
        100vh - var(--base-indent) - var(--base-indent) -
          var(--table-footer-height)
      );
      margin: var(--base-indent);
      background-color: var(--white);
      overflow: auto;
    }

    ReactDataSheet {
      background-color: var(--white);
    }
  `(
    <container>
      <content>
        <ReactDataSheet
          data={addOptionsToCell(grid, { readOnly: true })}
          valueRenderer={({ val, type, label, selected }) => {
            if (type === 'image')
              return (
                <LabeledCell selected={selected} label={label}>
                  <Image src={val} />
                </LabeledCell>
              );
            else
              return (
                <LabeledCell selected={selected} label={label}>
                  {val}
                </LabeledCell>
              );
          }}
          onSelect={handleSelect}
        />
      </content>
      {footer}
    </container>,
  );
};
